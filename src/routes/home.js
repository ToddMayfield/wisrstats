import nn from "../images/backgroundSubwayImage (1).png";
import "./home.css";
import launchIQ from "../images/launchIQ.PNG";
import enterReports from "../images/enterReports.PNG";
import companySalesReports from "../images/companySalesPic.PNG";
import exportSalesData from "../images/exportCSVFileSales.PNG";
import thirdPartyPage from "../images/thirdPartyPageImage.PNG";
import wisrSummary from "../images/enterWISRSummary.PNG";
import wisrSelectStore from "../images/wisrSelectStore.png";
import selectDateWISR from "../images/selectADate.png";
import hitExportWISR from "../images/hitExport.PNG";
import wisrStatPage from "../images/wisrStatPage.PNG";
import thirdPartyNumbered from "../images/3rdPartyPage.png";
import wisrPageNumbered from "../images/WISRStatsPage.png";

function showContent(sectionName) {
  var coll = document.getElementsByClassName(sectionName);
  var i;

  for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function () {
      this.classList.toggle("active");
      var content = this.nextElementSibling;
      if (content.style.display === "block") {
        content.style.display = "none";
      } else {
        content.style.display = "block";
      }
    });
  }
}

function Home() {
  return (
    <div>
      <div className="titleBarHome">
        The following website presents tools for:
        <div>
          <div> --- Displaying WISR details.</div>
          <div> --- Displaying Cost of Goods.</div>
          <div> --- Displaying Sale category percentages.</div>
          <div> --- Displaying Cost of Goods (Minus 3rd party royalty).</div>
        </div>
      </div>
      <div className="howToTitle"> How to use the WISR Stats page:</div>
      <button
        className="WISRStatCollapsible"
        onClick={() => showContent("WISRStatCollapsible")}
      >
        How to use the WISR Stats page: <i>Double-Click to expand</i>
      </button>

      <div className="content">
        <p>First we want to launch the LiveIQ portal.</p>
        <img src={launchIQ} alt="..."></img>
        <br />
        <br />
        <p>Next, we want to enter into the reports menu - WISR Summary.</p>
        <img src={wisrSummary} alt="..."></img>
        <br />
        <br />
        <p>Now we can select the required store if multiple are owned.</p>
        <img src={wisrSelectStore} alt="..."></img>
        <br />
        <br />
        <p>Select the desired weekending WISR.</p>
        <img src={selectDateWISR} alt="..."></img>
        <br />
        <br />
        <p>
          Lastly, we can export the data into a CSV file for use. It's a good
          idea to save this inside the corresponding WISR folder.
        </p>
        <img src={hitExportWISR} alt="..."></img>
        <br />
        <br />
        <p>
          Enter the WISR Stats page, and import the files to view your
          statistics.
        </p>
        <img src={wisrPageNumbered} alt="..."></img>
      </div>
      <br />
      <br />
      <div className="howToTitle"> How to use the 3rd Party Stats:</div>
      <button
        className="thridPartyCollapsible"
        onClick={() => showContent("thridPartyCollapsible")}
      >
        How to use the 3rd Party Stats page: <i>Double-Click to expand</i>
      </button>
      <div className="content">
        <p>
          {" "}
          If you haven't already, please follow the above "How to use the WISR
          Stats page. We will require those files too!
        </p>
        <p>First we want to launch the LiveIQ portal.</p>
        <img src={launchIQ} alt="..."></img>
        <br />
        <br />
        <p>
          Next, we want to enter into the reports menu - Extract / Printed .
        </p>
        <img src={enterReports} alt="..."></img>
        <br />
        <br />
        <p>Now we can navigate to the "Company Sales Details" page .</p>
        <img src={companySalesReports} alt="..."></img>
        <br />
        <br />
        <p>Lastly, we can export the data into a CSV file for use.</p>
        <img src={exportSalesData} alt="..."></img>
        <br />
        <br />
        <p>
          Enter the 3rd Party Stats page, and import the file to view your
          statistics.
        </p>
        <img src={thirdPartyNumbered} alt="..."></img>
      </div>
    </div>
  );
}
export default Home;
