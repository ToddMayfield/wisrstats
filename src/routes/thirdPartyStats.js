import React, { useState, useEffect } from "react";
//import { FileUploader } from "react-drag-drop-files";
import SaleCardList from "../components/saleCardList";
import "./thirdPartyStats.css";
import Search from "../components/search";
import imgtest from "../images/statBars.png";
import dollarIcon from "../images/dollarSignIcon.png";
//import DragDrop from "../components/dragDropFile";
import PieChart from "../components/pieChart";
import ChartFragment from "../components/chartFragment";
<link
  rel="stylesheet"
  href="cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"
>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</link>;
<link
  rel="stylesheet"
  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"
></link>;

function ThirdPartyStats() {
  const [file, setFile] = useState();
  const [itemArray, setArray] = useState([]);
  const { search } = window.location;
  const query = new URLSearchParams(search).get("query");
  const [searchQuery, setSearchQuery] = useState(query || "");
  const [filteredItems, setFilteredItems] = useState([]);
  const [ascOrder, setAscOrder] = useState();
  const [thirdPartyCost, setThirdPartyCost] = useState();
  const [totalSales, setTotalSales] = useState();
  var totalValue = 0;
  const [costOfGoods, setCostOfGoods] = useState([]);
  const [stringCOG, setStoreCOG] = useState("");
  const [amountCOG, setAmontCOG] = useState();
  var replaceCharacters = { '"': "", $: "", ",": "" };
  const [COGFile, setCOGFile] = useState();
  const fileTypes = ["CSV"];

  const thirdPartyTotals = [
    //Ingle farms system spelling
    "DEL-Menulog",
    "DEL-UBEREATS",
    "DEL-Doordash",
    "DEL-Deliveroo",
    //Glenelg spelling
    "DEL-UberEats",
    "DEL - MenuLog",
    "DEL - Deliveroo",
    "DEL - Doordash",
    //HT spelling
    "DEL-UberEats",
    "DEL - MenuLog",
    "DEL - DELIVEROO",
  ];

  const fileReader = new FileReader();

  /*Applies a search clause to populate matching results */
  const filterItems = (itemArray, queryString) => {
    if (!queryString) {
      return itemArray;
    }
    return itemArray.filter((item) => {
      if (item["tenderType"] === undefined) {
        return false;
      }
      if (item["tenderType"] !== undefined) {
        const itemName = item["tenderType"].toLowerCase();
        setTotalSales(item["amt2\r"]);

        window.sessionStorage.setItem(
          "totalSales",
          JSON.stringify(item["amt2\r"])
        );

        return itemName.includes(queryString.toLowerCase());
      }
      return false;
    });
  };

  //Populates the cost of goods percentage and dollar amount
  async function pullCostOfGoods(Items) {
    const CostOfGoodItem = ['"COST OF GOODS"'];

    var storeCOG = costOfGoods.filter((item) => {
      if (item['"CATEGORY SUMMARY"'] === undefined) {
        return false;
      }

      if (item['"CATEGORY SUMMARY"'] !== undefined) {
        const costingOfGoods = item['"CATEGORY SUMMARY"'];
        if (CostOfGoodItem.includes(costingOfGoods)) {
          var costingsOfGoods = item['"$ USED"'].replaceAll("$", "");
          costingsOfGoods = costingsOfGoods.replaceAll('"', "");
          costingsOfGoods = costingsOfGoods.replaceAll(",", "");
          var floatValueTotal = parseFloat(costingsOfGoods);

          setStoreCOG(item['"ACT %"']);
          setAmontCOG(floatValueTotal);

          window.sessionStorage.setItem("COGHolderDollar", amountCOG);
          window.sessionStorage.setItem("COGHolderPercent", stringCOG);
          //setTotalSales(item["amt2\r"]);

          //stringCOG.strip('"', "");
        }

        return CostOfGoodItem.includes(costingOfGoods);
      }
      return false;
    });
  }

  useEffect(() => {
    pullCostOfGoods(costOfGoods);
  });

  function sortItems(unsortedItems, ascOrder) {
    unsortedItems.sort(function (a, b) {
      if (a["tenderType"] !== undefined && b["tenderType"] !== undefined) {
        function getNonQuotes(s) {
          return s.replaceAll('"', "");
        }
        return getNonQuotes(a["tenderType"]).localeCompare(
          getNonQuotes(b["tenderType"])
        );
      }
      return false;
    });

    if (ascOrder !== true) {
      unsortedItems.reverse();
      setAscOrder(true);
    } else {
      setAscOrder(false);
    }
    var sortedItems = unsortedItems.map((x) => x);
    setFilteredItems(sortedItems);
  }

  function categoryTotals(Items) {
    totalValue = 0;
    var thirdParties = itemArray.filter((item) => {
      if (item["tenderType"] === undefined) {
        return false;
      }

      if (item["tenderType"] !== undefined) {
        const itemName = item["tenderType"];
        if (thirdPartyTotals.includes(itemName)) {
          var total = item["amt2\r"].replaceAll("$", "");
          total = total.replaceAll('"', "");
          total = total.replaceAll('"', "");
          total = total.replaceAll(",", "");

          var floatValueTotal = parseFloat(total);
          setTotalSales(floatValueTotal);
          window.sessionStorage.setItem(
            "totalSales",
            JSON.stringify(floatValueTotal)
          );

          //setThirdPartyCost("0");
          var priceHolder = item["amt"].replace(
            /[$",]/g,
            (m) => replaceCharacters[m]
          );

          priceHolder.replace(/$/g, "");
          priceHolder.replaceAll('"', "");
          priceHolder.replaceAll(",", "");

          priceHolder = parseFloat(priceHolder);
          totalValue += priceHolder;
        }

        return thirdPartyTotals.includes(itemName);
      }
      return false;
    });

    setThirdPartyCost(totalValue);

    window.sessionStorage.setItem("thirdPartyCost", JSON.stringify(totalValue));

    setFilteredItems(thirdParties);
  }

  const handleOnChange = (e) => {
    setFile(e.target.files[0]);
  };

  const csvFileToArray = (string) => {
    const WISRStore = string.slice(0, string.indexOf("\n")).split(",");
    const DateRange = string.slice(51, string.indexOf("\n") + 53).split(",");
    const csvHeader = string
      .slice(
        string.indexOf("Textbox160"),
        string.indexOf("\n") + (string.indexOf("amt2") - 14)
      )
      .split(",");
    const csvRows = string
      .slice(string.indexOf("Textbox160"), string.indexOf("\n") + 4500)
      .split("\n");
    const array = csvRows.map((i) => {
      var values = i.split(/(,)(?=(?:[^"]|"[^"]*")*$)/g);
      const obj = csvHeader.reduce((object, header, index) => {
        if (index === 0) {
          object[header] = values[index];
        }
        if (index === 1) {
          object[header] = values[index + 1];
        }
        if (index === 2) {
          object[header] = values[index + 2];
        }
        if (index === 3) {
          object[header] = values[index + 3];
        }
        if (index === 4) {
          object[header] = values[index + 4];
        }
        if (index === 5) {
          object[header] = values[index + 5];
        }
        if (index === 6) {
          object[header] = values[index + 6];
        }
        if (index === 7) {
          object[header] = values[index + 7];
        }
        if (index === 8) {
          object[header] = values[index + 8];
        }
        if (index === 9) {
          object[header] = values[index + 9];
        }
        if (index === 10) {
          object[header] = values[index + 10];
        }
        if (index === 11) {
          object[header] = values[index + 11];
        }

        return object;
      }, {});
      return obj;
    });
    setArray(array.slice(1, -2));
    window.sessionStorage.setItem(
      "array3rdHolder",
      JSON.stringify(array.slice(1, -2))
    );
  };

  //Pulls back in our item data on tab changes
  useEffect(() => {
    if (typeof window !== "undefined") {
      //necessary because u are using nextjs
      const storage = sessionStorage.getItem("array3rdHolder");
      if (storage) {
        setArray(JSON.parse(storage));
        //favs will be populated with your localStorage once, on component mount.
      }
    }
  }, []);

  //Pulls back in our item data on tab changes
  useEffect(() => {
    if (typeof window !== "undefined") {
      //necessary because u are using nextjs
      const storage = sessionStorage.getItem("thirdPartyCost");
      if (storage) {
        setThirdPartyCost(JSON.parse(storage));
        //favs will be populated with your localStorage once, on component mount.
      }
    }
  }, []);

  useEffect(() => {
    const storage = sessionStorage.getItem("thirdPartyCost");
    if (storage) {
      if (document.getElementsByClassName("spanner") !== null) {
        var value = document.getElementsByClassName("spanner")[0].textContent;

        if (value.includes("NaN%")) {
          categoryTotals(filteredItems);
        }
      } else {
        console.log("");
      }
    }
  });

  //Pulls back in COG costings and percentage
  useEffect(() => {
    if (typeof window !== "undefined") {
      //necessary because u are using nextjs
      const storageDollar = sessionStorage.getItem("COGHolderDollar");
      if (storageDollar) {
        setAmontCOG(storageDollar);
        //favs will be populated with your localStorage once, on component mount.
      }
      const storagePercent = sessionStorage.getItem("COGHolderPercent");
      if (storagePercent) {
        setStoreCOG(storagePercent);
      }
    }
  }, []);

  const handleOnSubmit = (e) => {
    e.preventDefault();
    // sortItems(filteredItems);

    if (file) {
      fileReader.onload = function (event) {
        const text = event.target.result;
        csvFileToArray(text);
      };

      fileReader.readAsText(file);
    }
  };
  const COGCvsFileToArray = (string) => {
    const csvHeader = string.slice(47, string.indexOf("\n") + 93).split(",");
    const csvRows = string.slice(string.indexOf("\n") + 30).split("\n");
    const storeCOG = string.slice(string.indexOf("\n") + 30).split("\n");
    setCostOfGoods(storeCOG);
    const array = csvRows.map((i) => {
      var values = i.split(/(,)(?=(?:[^"]|"[^"]*")*$)/g);
      const obj = csvHeader.reduce((object, header, index) => {
        if (index === 0) {
          object[header] = values[index];
        }
        if (index === 1) {
          object[header] = values[index + 1];
        }
        if (index === 2) {
          object[header] = values[index + 2];
        }
        if (index === 3) {
          object[header] = values[index + 3];
        }
        if (index === 4) {
          object[header] = values[index + 4];
        }

        return object;
      }, {});
      return obj;
    });

    setCostOfGoods(array.slice(1, -1));
  };

  const handleOnSubmitCOG = (e) => {
    e.preventDefault();

    if (COGFile) {
      fileReader.onload = function (event) {
        const text = event.target.result;
        COGCvsFileToArray(text);
        e.preventDefault();
        pullCostOfGoods(costOfGoods);
      };

      fileReader.readAsText(COGFile);
    }
  };

  useEffect(() => {
    setFilteredItems(filterItems(itemArray, searchQuery));
  }, [itemArray, searchQuery]);

  const handleOnChangeCOG = (e) => {
    setCOGFile(e.target.files[0]);
  };

  const headerKeys = Object.keys(Object.assign({}, ...itemArray));

  return (
    <div style={{ textAlign: "center" }}>
      <div className="titleBar">
        <h2> Third Party Stats and Cost of Goods </h2>
      </div>
      <div className="TopBar">
        <div className="fileImports">
          Sales Details File
          <form>
            <input
              className="buttonOutLine"
              type={"file"}
              id={"csvFileInput"}
              accept={".csv"}
              onChange={handleOnChange}
            />
            &nbsp;
            <button
              className="buttonOutLine"
              onClick={(e) => {
                handleOnSubmit(e);
              }}
            >
              IMPORT FILE
            </button>
          </form>
          Inventory Category File
          <form>
            <input
              className="buttonOutLine"
              type={"file"}
              id={"COGCsvFileInput"}
              accept={".csv"}
              onChange={handleOnChangeCOG}
            />
            &nbsp;
            <button
              className="buttonOutLine"
              onClick={(e) => {
                handleOnSubmitCOG(e);
              }}
            >
              IMPORT FILE
            </button>
          </form>
          Filter &nbsp;
          <button
            className="buttonOutLine"
            onClick={() => categoryTotals(filteredItems)}
          >
            Show 3rd Party
          </button>
        </div>
        <div className="searchingSection">
          <Search
            searchQuery={searchQuery}
            setSearchQuery={setSearchQuery}
            handleOnChange={handleOnChange}
            handleOnSubmit={handleOnSubmit}
          ></Search>
        </div>
      </div>
      <br />
      <div>
        <div className="warningFile">
          {thirdPartyCost === undefined || amountCOG === undefined
            ? "Please import both files above and click filter by 3rd Party. Check the main page for help downloading these files."
            : ""}
        </div>
        <div className="main">
          <div className="wrapper">
            <div className="box a">
              <PieChart
                className="chartSection"
                pieValues={filteredItems}
              ></PieChart>
            </div>
            <ChartFragment pieValues={filteredItems}></ChartFragment>
            {/* <div className="box b"></div> */}
            <div className="box c">
              {thirdPartyCost !== undefined ? (
                <div className="dashboard-stat">
                  <img className="visual" src={dollarIcon} alt="..."></img>
                  <i className="fa fa-bar-chart-o"></i>

                  <div className="details">
                    <div className="number">
                      <span>${thirdPartyCost}</span>
                    </div>
                    <div className="desc">3PD Total</div>
                  </div>
                </div>
              ) : (
                <div className="dashboard-stat">
                  <img className="visual" src={dollarIcon} alt="..."></img>
                  <i className="fa fa-bar-chart-o"></i>

                  <div className="details">
                    <div className="number">
                      <span>$</span>
                    </div>
                    <div className="desc">3PD Total</div>
                  </div>
                </div>
              )}
            </div>
            <div className="box d">
              {amountCOG !== undefined ? (
                <div className="dashboard-stat">
                  <img className="visual" src={dollarIcon} alt="..."></img>
                  <i className="fa fa-bar-chart-o"></i>

                  <div className="details">
                    <div className="number">
                      <span>
                        {(
                          (amountCOG /
                            (totalSales * 0.9 - thirdPartyCost * 0.3)) *
                          100
                        ).toFixed(2) + "%"}
                      </span>
                    </div>
                    <div className="desc">3PD COG</div>
                  </div>
                </div>
              ) : (
                <div className="dashboard-stat">
                  <img className="visual" src={dollarIcon} alt="..."></img>
                  <i className="fa fa-bar-chart-o"></i>

                  <div className="details">
                    <div className="number">
                      <span>$</span>
                    </div>
                    <div className="desc">3PD COG</div>
                  </div>
                </div>
              )}
            </div>
            <div className="box e">e</div>
            <div className="box f">
              {filterItems !== undefined ? (
                <div className="dashboard-stat">
                  <img className="visual" src={imgtest} alt="..."></img>
                  <i className="fa fa-bar-chart-o"></i>

                  <div className="details">
                    <div className="number">
                      <span className="spanner">
                        {" "}
                        {((thirdPartyCost / totalSales) * 100)
                          .toFixed(2)
                          .toString() + "%"}
                      </span>
                    </div>
                    <div className="desc">3PD Percent</div>
                  </div>
                </div>
              ) : (
                <h3 className="statSideCard">
                  3PD Percentage: <br /> %
                </h3>
              )}
            </div>
            {/* <div className="box g">g</div>
            <div className="box h">h</div> */}
            <div className="box i">
              {amountCOG !== undefined ? (
                <div className="dashboard-stat">
                  <img className="visual" src={dollarIcon} alt="..."></img>
                  <i className="fa fa-bar-chart-o"></i>

                  <div className="details">
                    <div className="number">
                      <span>${amountCOG}</span>
                    </div>
                    <div className="desc">COG Total</div>
                  </div>
                </div>
              ) : (
                <div className="dashboard-stat">
                  <img className="visual" src={dollarIcon} alt="..."></img>
                  <i className="fa fa-bar-chart-o"></i>

                  <div className="details">
                    <div className="number">
                      <span>$</span>
                    </div>
                    <div className="desc">COG Total</div>
                  </div>
                </div>
              )}
            </div>
            <div className="box j">
              {costOfGoods !== undefined ? (
                <div className="dashboard-stat">
                  <img className="visual" src={imgtest} alt="..."></img>
                  <i className="fa fa-bar-chart-o"></i>

                  <div className="details">
                    <div className="number">
                      <span>{stringCOG}%</span>
                    </div>
                    <div className="desc">COG Percent</div>
                  </div>
                </div>
              ) : (
                <h3 className="statSideCard">
                  3PD Percentage: <br /> %
                </h3>
              )}
            </div>
          </div>

          <SaleCardList saleCardData={filteredItems} />
          <br />
          <br />
          <table className="tableStats">
            <thead>
              <tr key={"header"}>
                {headerKeys.map((key, index) => (
                  <th key={index}>{key}</th>
                ))}
              </tr>
            </thead>

            <tbody>
              {filteredItems.map((item, index) => (
                <tr key={index}>
                  {Object.values(item).map((val) => (
                    <td key={(index += 1)}>{val}</td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
export default ThirdPartyStats;
