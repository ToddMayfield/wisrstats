import React, { useState, useEffect } from "react";
import CardList from "../components/cardList";
import Search from "../components/search";
import "./wisrStats.css";
import nn from "../images/backgroundSubwayImage (1).png";

function WisrStats() {
  const [file, setFile] = useState();
  const [itemArray, setArray] = useState([]);
  const [stringStore, setStore] = useState();
  const [weekEnding, setWeekEnding] = useState();
  const [COGFile, setCOGFile] = useState();
  const [costOfGoods, setCostOfGoods] = useState([]);
  const [stringCOG, setStoreCOG] = useState();
  const [amountCOG, setAmontCOG] = useState();

  const { search } = window.location;
  const query = new URLSearchParams(search).get("query");
  const [searchQuery, setSearchQuery] = useState(query || "");
  const [filteredItems, setFilteredItems] = useState([]);
  const [ascOrder, setAscOrder] = useState();

  const TopTenItems = [
    '"""Chicken Strips - Metric"""',
    '"""Meatballs - Metric"""',
    '"""Bread"""',
    '"""Olives - Metric"""',
    '"""Lettuce - Metric"""',
    '"""Tuna - Metric"""',
    '"""Cookies"""',
    '"""Napkins"""',
    '"""Toppings Sub Total"""',
    '"""Cheese, Shredded, Mozz..."""',
  ];

  const categoryTotalItems = [
    '"""Cheese Sub Total"""',
    '"""Meat Protein Sub Total"""',
    '"""Bread Carrier Sub Total"""',
    '"""Veggies Sub Total"""',
    '"""Toppings Sub Total"""',
    '"""Papergoods Sub Total"""',
    '"""Drinks Total"""',
    '"""Snacks Sub Total"""',
  ];

  /*Applies a search clause to populate matching results */
  const filterItems = (itemArray, queryString) => {
    if (!queryString) {
      return itemArray;
    }
    return itemArray.filter((item) => {
      if (item['"Item"'] === undefined) {
        return false;
      }
      if (item['"Item"'] !== undefined) {
        const itemName = item['"Item"'].toLowerCase();
        return itemName.includes(queryString.toLowerCase());
      }
      return false;
    });
  };

  /*Populates the category totals for product types (cheese, meat, etc.) */
  function categoryTotals(Items) {
    var topTen = itemArray.filter((item) => {
      if (item['"Item"'] === undefined) {
        return false;
      }

      if (item['"Item"'] !== undefined) {
        const itemName = item['"Item"'];

        return categoryTotalItems.includes(itemName);
      }
      return false;
    });
    setFilteredItems(topTen);
  }

  //Populates the cost of goods percentage and dollar amount
  async function pullCostOfGoods(Items) {
    var storeCOG = costOfGoods.filter((item) => {
      const CostOfGoodItem = ['"COST OF GOODS"'];
      if (item['"CATEGORY SUMMARY"'] === undefined) {
        return false;
      }

      if (item['"CATEGORY SUMMARY"'] !== undefined) {
        const costingOfGoods = item['"CATEGORY SUMMARY"'];
        if (CostOfGoodItem.includes(costingOfGoods)) {
          setStoreCOG(item['"ACT %"']);
          setAmontCOG(item['"$ USED"']);
          //amountCOG();
          if (amountCOG !== undefined && stringCOG !== undefined) {
            amountCOG.replaceAll('"', "");
            stringCOG.replaceAll('"', "");
            window.sessionStorage.setItem("COGHolderDollar", amountCOG);
            window.sessionStorage.setItem("COGHolderPercent", stringCOG);
          }
        }
        return CostOfGoodItem.includes(costingOfGoods);
      }
      return false;
    });
  }

  /*Populates the top 10 at risk items to watch */
  function top10Items(Items) {
    var topTen = itemArray.filter((item) => {
      if (item['"Item"'] === undefined) {
        return false;
      }

      if (item['"Item"'] !== undefined) {
        const itemName = item['"Item"'];
        return TopTenItems.includes(itemName);
      }
      return false;
    });
    setFilteredItems(topTen);
  }

  function sortItems(unsortedItems, ascOrder) {
    unsortedItems.sort(function (a, b) {
      if (a['"Item"'] !== undefined && b['"Item"'] !== undefined) {
        function getNonQuotes(s) {
          return s.replaceAll('"', "");
        }
        return getNonQuotes(a['"Item"']).localeCompare(
          getNonQuotes(b['"Item"'])
        );
      }
      return false;
    });

    if (ascOrder !== true) {
      unsortedItems.reverse();
      setAscOrder(true);
    } else {
      setAscOrder(false);
    }
    var sortedItems = unsortedItems.map((x) => x);
    setFilteredItems(sortedItems);
  }

  useEffect(() => {
    pullCostOfGoods(costOfGoods);
  });

  useEffect(() => {
    setFilteredItems(filterItems(itemArray, searchQuery));
  }, [itemArray, searchQuery]);

  const fileReader = new FileReader();

  const handleOnChange = (e) => {
    setFile(e.target.files[0]);
  };

  const handleOnChangeCOG = (e) => {
    setCOGFile(e.target.files[0]);
  };

  const COGCvsFileToArray = (string) => {
    const csvHeader = string.slice(47, string.indexOf("\n") + 93).split(",");
    const csvRows = string.slice(string.indexOf("\n") + 30).split("\n");
    const storeCOG = string.slice(string.indexOf("\n") + 30).split("\n");
    setCostOfGoods(storeCOG);

    const array = csvRows.map((i) => {
      var values = i.split(/(,)(?=(?:[^"]|"[^"]*")*$)/g);
      const obj = csvHeader.reduce((object, header, index) => {
        if (index === 0) {
          object[header] = values[index];
        }
        if (index === 1) {
          object[header] = values[index + 1];
        }
        if (index === 2) {
          object[header] = values[index + 2];
        }
        if (index === 3) {
          object[header] = values[index + 3];
        }
        if (index === 4) {
          object[header] = values[index + 4];
        }

        return object;
      }, {});
      return obj;
    });

    setCostOfGoods(array.slice(1, -1));
  };

  const csvFileToArray = (string) => {
    const WISRStore = string.slice(0, string.indexOf("\n")).split(",");
    const WISRDate = string.slice(18, string.indexOf("\n") + 30).split(",");
    setStore(string.slice(8, string.indexOf("\n")).split(","));
    setWeekEnding(string.slice(32, string.indexOf("\n") + 30).split(","));

    window.sessionStorage.setItem(
      "storeNumber",
      string.slice(8, string.indexOf("\n")).split(",")
    );
    window.sessionStorage.setItem(
      "weekEnding",
      string.slice(32, string.indexOf("\n") + 30).split(",")
    );

    const csvHeader = string.slice(47, string.indexOf("\n") + 131).split(",");
    const csvRows = string.slice(string.indexOf("\n") + 30).split("\n");

    const array = csvRows.map((i) => {
      var values = i.split(/(,)(?=(?:[^"]|"[^"]*")*$)/g);
      const obj = csvHeader.reduce((object, header, index) => {
        if (index === 0) {
          object[header] = values[index];
        }
        if (index === 1) {
          object[header] = values[index + 1];
        }
        if (index === 2) {
          object[header] = values[index + 2];
        }
        if (index === 3) {
          object[header] = values[index + 3];
        }
        if (index === 4) {
          object[header] = values[index + 4];
        }
        if (index === 5) {
          object[header] = values[index + 5];
        }
        if (index === 6) {
          object[header] = values[index + 6];
        }
        if (index === 7) {
          object[header] = values[index + 7];
        }
        if (index === 8) {
          object[header] = values[index + 8];
        }
        if (index === 9) {
          object[header] = values[index + 9];
        }
        if (index === 10) {
          object[header] = values[index + 10];
        }
        if (index === 11) {
          object[header] = values[index + 11];
        }

        return object;
      }, {});
      return obj;
    });

    setArray(array.slice(1, -1));
    console.log(array.slice(1, -1));
    window.sessionStorage.setItem(
      "arrayHolder",
      JSON.stringify(array.slice(1, -1))
    );

    // let holder = JSON.parse(window.sessionStorage.getItem("arrayHolder"));
    // console.log(holder);
  };

  //Pulls back in our item data on tab changes
  useEffect(() => {
    if (typeof window !== "undefined") {
      //necessary because u are using nextjs
      const storage = sessionStorage.getItem("arrayHolder");
      if (storage) {
        setArray(JSON.parse(storage));
        //favs will be populated with your localStorage once, on component mount.
      }
    }
  }, []);

  //Pulls back in our store number and date data
  useEffect(() => {
    if (typeof window !== "undefined") {
      //necessary because u are using nextjs
      const storageStore = sessionStorage.getItem("storeNumber");
      if (storageStore) {
        setStore(storageStore);
        //favs will be populated with your localStorage once, on component mount.
      }
      const dateStorage = sessionStorage.getItem("weekEnding");
      if (dateStorage) {
        setWeekEnding(dateStorage);
      }
    }
  }, []);

  //Pulls back in our store number and date data
  useEffect(() => {
    if (typeof window !== "undefined") {
      //necessary because u are using nextjs
      const storageDollar = sessionStorage.getItem("COGHolderDollar");
      if (storageDollar) {
        setAmontCOG(storageDollar);
        //favs will be populated with your localStorage once, on component mount.
      }
      const storagePercent = sessionStorage.getItem("COGHolderPercent");
      if (storagePercent) {
        setStoreCOG(storagePercent);
      }
    }
  }, []);

  const handleOnSubmitCOG = (e) => {
    e.preventDefault();

    if (COGFile) {
      fileReader.onload = function (event) {
        const text = event.target.result;
        COGCvsFileToArray(text);

        e.preventDefault();
        pullCostOfGoods(costOfGoods);
      };

      fileReader.readAsText(COGFile);
    }
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    sortItems(filteredItems);

    if (file) {
      fileReader.onload = function (event) {
        const text = event.target.result;
        csvFileToArray(text);
      };

      fileReader.readAsText(file);
    }
  };

  const headerKeys = Object.keys(Object.assign({}, ...itemArray));

  // if (localStorage.inventoryItemsFile !== null) {
  //   document
  //     .getElementById("inventItemFile")
  //     .value(localStorage.inventoryItemsFile);
  // }

  // console.log("Hmmmmmm");

  // if (localStorage.getItem("itemArray") !== null && itemArray.length === 0) {
  //   setArray(JSON.parse(localStorage.getItem("itemArray")));
  //   console.log(itemArray.length);
  //   console.log("working");
  //   console.log(localStorage.getItem("itemArray"));
  // } else {
  //   // console.log(itemArray);
  //   console.log("meow");
  //   // console.log("local below");
  //   // console.log(sessionStorage.getItem("itemArray"));
  // }

  // // if (localStorage.getItem("filterItems") !== null && filterItems.length === 0) {
  // //   setFilteredItems(JSON.parse(localStorage.getItem("filterItems")));
  // // }

  return (
    <div style={{ textAlign: "center" }}>
      <div className="titleBar">
        <h2> WISR Statistics and Cost of Goods</h2>
      </div>
      <div className="TopBar">
        <div className="fileImports">
          Inventory Items File
          <form title="Select the InventoryItems File">
            <input
              className="buttonOutLine"
              type={"file"}
              id={"csvFileInput"}
              accept={".csv"}
              onChange={handleOnChange}
            />
            &nbsp;
            <button
              className="buttonOutLine"
              onClick={(e) => {
                handleOnSubmit(e);
              }}
            >
              IMPORT FILE
            </button>
          </form>
          Inventory Category File
          <form title="Select the InventoryCategoriesSummary File">
            <input
              className="buttonOutLine"
              type={"file"}
              id={"COGCsvFileInput"}
              accept={".csv"}
              onChange={handleOnChangeCOG}
            />
            &nbsp;
            <button
              className="buttonOutLine"
              onClick={(e) => {
                handleOnSubmitCOG(e);
              }}
            >
              IMPORT FILE
            </button>
          </form>
          <div>
            Filter &nbsp;
            <button
              title="Shows the 10 highest at risk items"
              className="buttonOutLine"
              onClick={() => top10Items(filteredItems)}
            >
              Show Top Ten Items
            </button>
            &nbsp;
            <button
              title="Filter by category totals"
              className="buttonOutLine"
              onClick={() => categoryTotals(filteredItems)}
            >
              Show Item Category Totals
            </button>
            &nbsp;
            <button
              title="Sorts A -> Z and Z -> A"
              className="buttonOutLine"
              onClick={() => sortItems(filteredItems, ascOrder)}
            >
              Toggle Sort Items (Alphabetical)
            </button>
          </div>
        </div>
        <div className="searchingSection">
          <Search
            searchQuery={searchQuery}
            setSearchQuery={setSearchQuery}
            handleOnChange={handleOnChange}
            handleOnSubmit={handleOnSubmit}
          ></Search>
        </div>
      </div>
      <br />
      <div></div>

      <div className="main" background={{ backgroundImage: `url(${nn})` }}>
        <span>
          <div key={"header"}>
            {stringStore !== undefined || weekEnding !== undefined ? (
              <h3>
                Store: {stringStore} Week Ending {weekEnding}
              </h3>
            ) : (
              "Store: N/A  \u00A0 \u00A0 \u00A0    Week Ending: N/A"
            )}
            {stringCOG !== undefined && amountCOG !== undefined
              ? "Cost of Goods: " +
                stringCOG.replaceAll('"', "") +
                "%" +
                " / $" +
                amountCOG.replaceAll('"', "")
              : "\u00A0 \u00A0 \u00A0 Cost of Goods: N/A% \u00A0 / \u00A0 $N/A "}
          </div>
          <div className="warningFile">
            {stringStore === undefined || amountCOG === undefined
              ? "Please import both files above, check the main page for help locating them."
              : ""}
          </div>
        </span>
        <CardList cardData={filteredItems} />
        <br />
        <br />

        <table className="tableStats">
          <thead>
            <tr key={"header"}>
              {headerKeys.map((key, index) => (
                <th key={index}>{key}</th>
              ))}
            </tr>
          </thead>

          <tbody>
            {filteredItems.map((item, index) => (
              <tr key={index}>
                {Object.values(item).map((val) => (
                  <td key={(index += 1)}>{val}</td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
export default WisrStats;
