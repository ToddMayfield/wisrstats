import React, { useState, useEffect, Suspense, lazy } from "react";
import CardList from "./components/cardList";
import Search from "./components/search";
import Navbar from "./components/navbar";
import "./App.css";
import { findRenderedDOMComponentWithTag } from "react-dom/test-utils";
import subwayLogo from "./images/subway-logo-new.png";
import nn from "./images/backgroundSubwayImage (1).png";
//import backgroundLogo from "./images/backgroundSubwayImage (1).png";

import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";

const Home = lazy(() => import("./routes/home"));
const About = lazy(() => import("./routes/about"));
const WisrStats = lazy(() => import("./routes/WisrStats"));

function App() {
  return (
    <div style={{ backgroundImage: `url(${nn})` }}>
      <div className="topBar">
        <div className="App-logo">
          <img src={subwayLogo} alt="..."></img>
        </div>
        <Navbar></Navbar>
      </div>
    </div>
  );
}
export default App;
