const SearchBar = ({
  searchQuery,
  setSearchQuery,
  handleOnChange,
  handleOnSubmit,
}) => {
  return (
    <form action="/" method="get" autoComplete="off">
      <label htmlFor="header-search">
        <span className="visually-hidden">Search Subway Items &nbsp;</span>
      </label>
      {/*Submit button pushes search to url eg. ?query={searchbar input} */}
      <input
        value={searchQuery}
        onInput={(e) => setSearchQuery(e.target.value)}
        onChange={handleOnChange}
        onClick={(e) => {
          handleOnSubmit(e);
        }}
        type="text"
        id="header-search"
        placeholder="Seach Subway Items"
        name="searchText"
      />
      {/* <button type="submit">Search</button> */}
      <br />
    </form>
  );
};

export default SearchBar;
