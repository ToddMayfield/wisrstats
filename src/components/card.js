import React from "react";
import "./card.css";
<script
  src="https://kit.fontawesome.com/[myid].js"
  crossorigin="anonymous"
></script>;

function upOrDown(item) {
  if (item !== undefined) {
    var itemEdit = item.replaceAll('"', "");
  }
  return itemEdit;
}

class Card extends React.Component {
  render() {
    return (
      <div className="card">
        <div className="card-header">
          <div className="profile">
            <span className="letter">
              {" "}
              {this.props.itemName !== undefined
                ? this.props.itemName.charAt(3).toUpperCase()
                : this.props.itemName}
            </span>
          </div>

          <div className="card-title-group">
            <h5 className="card-title">
              {" "}
              {this.props.itemName !== undefined
                ? this.props.itemName.replaceAll('"""', "")
                : this.props.itemName}
            </h5>
          </div>
        </div>
        {Math.sign(Number(upOrDown(this.props.diffCost))) < 0 ? (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="redBoi">
              <i className="fa fa-arrow-up"></i>
              Amount: &#9660; {upOrDown(this.props.posDiff)}
            </span>
          </p>
        ) : Math.sign(Number(upOrDown(this.props.diffCost))) === 0 ? (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="yellowBoi">
              <i className="fa fa-arrow-up"></i>
              Amount: &#9654; {upOrDown(this.props.posDiff)}
            </span>
          </p>
        ) : (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="greenBoi">
              <i className="fa fa-arrow-up"></i>
              Amount: &#9650; {upOrDown(this.props.posDiff)}
            </span>
          </p>
        )}

        {Math.sign(Number(upOrDown(this.props.diffCost))) < 0 ? (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="redBoi">
              <i className="fa fa-arrow-up"></i>
              Cost: &#9660; ${upOrDown(this.props.diffCost)}
            </span>
          </p>
        ) : Math.sign(Number(upOrDown(this.props.diffCost))) === 0 ? (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="yellowBoi">
              <i className="fa fa-arrow-up"></i>
              Cost: &#9654; ${upOrDown(this.props.diffCost)}
            </span>
          </p>
        ) : (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="greenBoi">
              <i className="fa fa-arrow-up"></i>
              Cost: &#9650; ${upOrDown(this.props.diffCost)}
            </span>
          </p>
        )}
        {/* <div className="card-text">Opened on: {this.props.openAmount}</div>
        <div className="card-text">Amount Delivered: {this.props.del}</div>
        <div className="card-text">Amount Remaining: {this.props.left}</div>
        <div className="card-text">Amount Used: {this.props.used}</div>
        <div className="card-text">POS Difference: {this.props.posDiff}</div>
        <div className="card-cost">Cost: $ {this.props.cost}</div>
        <div className="card-cost">Usage Cost: $ {this.props.usedCost}</div>
        <div className="card-cost">Diff Cost: $ {this.props.diffCost}</div> */}
        {Math.sign(Number(upOrDown(this.props.diffCost))) < 0 ? (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="redBoi">
              <i className="fa fa-arrow-up"></i>
              Expected: &#9660; {upOrDown(this.props.percentDiff)}%
            </span>
          </p>
        ) : Math.sign(Number(upOrDown(this.props.diffCost))) === 0 ? (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="yellowBoi">
              <i className="fa fa-arrow-up"></i>
              Expected: &#9654; {upOrDown(this.props.percentDiff)}%
            </span>
          </p>
        ) : (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="greenBoi">
              <i className="fa fa-arrow-up"></i>
              Expected: &#9650; {upOrDown(this.props.percentDiff)}%
            </span>
          </p>
        )}
      </div>
    );
  }
}

export default Card;
