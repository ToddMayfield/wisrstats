import { React, Suspense, lazy } from "react";
import "./navbar.css";

import subwayLogo from "../images/subway-logo-new.png";

import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";

const Home = lazy(() => import("../routes/home"));
const About = lazy(() => import("../routes/about"));
const WisrStats = lazy(() => import("../routes/WisrStats"));
const ThirdPartyStats = lazy(() => import("../routes/thirdPartyStats"));

function Navbar() {
  return (
    <div>
      <Router>
        <nav style={{ textAlign: "center" }}>
          <ul>
            <li>
              <Link to="/home">Home</Link>
              <Link to="/about">About</Link>
              <Link to="/WisrStats">WISR Stats</Link>
              <Link to="/ThirdPartyStats">3rd Party Stats</Link>
            </li>
          </ul>
        </nav>
        <Suspense fallback={<div>Loading...</div>}>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route path="/home" element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/WisrStats" element={<WisrStats />} />
            <Route path="/ThirdPartyStats" element={<ThirdPartyStats />} />
          </Routes>
        </Suspense>
      </Router>
    </div>
  );
}
export default Navbar;
