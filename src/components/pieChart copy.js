import React from "react";
import { PieChart } from "react-minimal-pie-chart";
import "./pieChart.css";

function parseInput(value) {
  if (value !== undefined) {
    value = value.replaceAll("$", "");
    value = value.replaceAll('"', "");
    value = value.replaceAll(",", "");
    value = Math.round(parseFloat(value));
    //value = parseFloat(value);
  }

  return value;
}

function removeQoutations(value) {
  if (value !== undefined) {
    value = value.replaceAll("'", "");
  }

  return value;
}

//Holds a number of cards in a list, fed from json data.
class PieCharter extends React.Component {
  render() {
    var colours = [
      "#E38627",
      "#7ad17d",
      "#ed091e",
      "#b060db",
      "#3b44b3",
      "#ca4883",
      "#2a59fc",
      "#19e957",
      "#cf1a26",
      "#f5848c",
      "#55a6f7",
      "#29698a",
      "#01a17c",
      "#bb604d",
      "#77c404",
      "#5d0a55",
      "#186626",
      "#65a3c4",
      "#fbfc4a",
      "#e37b43",
      "#1846e3",
      "#f65457",
      "#fe69b5",
      "#6A2135",
      "#E38627",
      "#C13C37",
    ];

    // let charting = (
    // <PieChart
    //     data={this.props.pieValues.map((saleCard) => (
    //         {
    //             [
    //                 { title: saleCard["tenderType"], value: saleCard["amt"], color: colours[0] },
    //             ]
    //         }

    //     ))
    //     }

    // />
    // );

    let data = this.props.pieValues.map((saleCard, index) => ({
      // color: colours[index],
      // value: parseInput(saleCard["amt"]),
      // key: saleCard["tenderType"],
      // title: saleCard["tenderType"],
      title: removeQoutations(saleCard["tenderType"]),
      color: colours[index],
      value: parseInput(saleCard["amt"]),
    }));

    // id={saleCard["tenderType"]}
    // itemName={saleCard["tenderType"]}
    // saleAmount={saleCard["amt"]}
    // totalSales={saleCard["amt2\r"]}
    //      percentageOfTotal={saleCard["Percentage"]}

    // for (let i = 0; i < this.props.pieValues.length; i++) {
    //   data += {
    //     color: "#E38627",
    //     value: this.props.pieValues["amt"],
    //     title: this.props.pieValue["tenderType"],
    //   };

    let charting = (
      <div>
        <PieChart
          animate
          animationDuration={500}
          animationEasing="ease-out"
          center={[100, 100]}
          data={data}
          lengthAngle={360}
          //lineWidth={100}
          //paddingAngle={0}
          radius={50}
          //rounded
          startAngle={0}
          viewBoxSize={[800, 160]}
          label={(data) => data.dataEntry.value}
          //labelPosition={65}
          // labelStyle={{
          //   fontSize: "5px",
          //   fontColor: "FFFFFA",
          //   fontWeight: "800",
          // }}
          paddingAngle={0}
          labelStyle={{
            fontSize: "10px",
            fill: "#000",
          }}
          labelPosition={105}
          lineWidth={40}
        />
        <>
          <div className="holder" onChange="box b.appendChild(this)">
            {data.map((displayData) => (
              <div
                className="pieStatLegend"
                key={displayData.title}
                style={{ color: displayData.color }}
              >
                &#9632; {displayData.title}
              </div>
            ))}
            {}
          </div>
        </>
      </div>
    );

    return <div>{charting}</div>;
  }
}

export default PieCharter;
