import React from "react";
import "./saleCard.css";
<script
  src="https://kit.fontawesome.com/[myid].js"
  crossorigin="anonymous"
></script>;

function upOrDown(item) {
  if (item !== undefined) {
    var itemEdit = item.replaceAll('"', "");
  }
  return itemEdit;
}

class SaleCard extends React.Component {
  render() {
    return (
      <div className="saleCard">
        <div className="saleCard-header">
          <div className="profile">
            <span className="letter">
              {" "}
              {this.props.itemName !== undefined
                ? this.props.itemName.charAt(0).toUpperCase()
                : this.props.itemName}
            </span>
          </div>

          <div className="saleCard-title-group">
            <h5 className="saleCard-title">
              {" "}
              {this.props.itemName !== undefined
                ? this.props.itemName.replaceAll('"""', "")
                : this.props.itemName}
            </h5>
          </div>
        </div>
        {Math.sign(Number(upOrDown(this.props.saleAmount))) < 0 ? (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="redBoi">
              <i className="fa fa-arrow-up"></i>
              Amount: &#9660; {upOrDown(this.props.saleAmount)}
            </span>
          </p>
        ) : Math.sign(Number(upOrDown(this.props.saleAmount))) === 0 ? (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="yellowBoi">
              <i className="fa fa-arrow-up"></i>
              Amount: &#9654; {upOrDown(this.props.saleAmount)}
            </span>
          </p>
        ) : (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="greenBoi">
              <i className="fa fa-arrow-up"></i>
              Amount: &#9650; {upOrDown(this.props.saleAmount)}
            </span>
          </p>
        )}

        {Math.sign(Number(upOrDown(this.props.percentageOfTotal))) < 0 ? (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="redBoi">
              <i className="fa fa-arrow-up"></i>
              Percentage: &#9660; ${upOrDown(this.props.percentageOfTotal)}
            </span>
          </p>
        ) : Math.sign(Number(upOrDown(this.props.percentageOfTotal))) === 0 ? (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="yellowBoi">
              <i className="fa fa-arrow-up"></i>
              Percentage: &#9654; ${upOrDown(this.props.percentageOfTotal)}
            </span>
          </p>
        ) : (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="greenBoi">
              <i className="fa fa-arrow-up"></i>
              Percentage: &#9650; ${upOrDown(this.props.percentageOfTotal)}
            </span>
          </p>
        )}
        {/* <div className="card-text">Opened on: {this.props.openAmount}</div>
        <div className="card-text">Amount Delivered: {this.props.del}</div>
        <div className="card-text">Amount Remaining: {this.props.left}</div>
        <div className="card-text">Amount Used: {this.props.used}</div>
        <div className="card-text">POS Difference: {this.props.posDiff}</div>
        <div className="card-cost">Cost: $ {this.props.cost}</div>
        <div className="card-cost">Usage Cost: $ {this.props.usedCost}</div>
        <div className="card-cost">Diff Cost: $ {this.props.diffCost}</div> */}
        {Math.sign(Number(upOrDown(this.props.totalSales))) < 0 ? (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="redBoi">
              <i className="fa fa-arrow-up"></i>
              Total: &#9660; {upOrDown(this.props.totalSales)}
            </span>
          </p>
        ) : Math.sign(Number(upOrDown(this.props.totalSales))) === 0 ? (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="yellowBoi">
              <i className="fa fa-arrow-up"></i>
              Total: &#9654; {upOrDown(this.props.totalSales)}
            </span>
          </p>
        ) : (
          <p className="mt-3 mb-0 text-muted text-sm">
            <span className="greenBoi">
              <i className="fa fa-arrow-up"></i>
              Total: &#9650; {upOrDown(this.props.totalSales)}
            </span>
          </p>
        )}
      </div>
    );
  }
}

export default SaleCard;
