import React from "react";
import { PieChart } from "react-minimal-pie-chart";
import "./pieChart.css";

function parseInput(value) {
  if (value !== undefined) {
    value = value.replaceAll("$", "");
    value = value.replaceAll('"', "");
    value = value.replaceAll(",", "");
    value = Math.round(parseFloat(value));
    //value = parseFloat(value);
  }

  return value;
}

function removeQoutations(value) {
  if (value !== undefined) {
    value = value.replaceAll("'", "");
  }

  return value;
}

//Holds a number of cards in a list, fed from json data.
class ChartFragment extends React.Component {
  render() {
    var colours = [
      "#E38627",
      "#7ad17d",
      "#ed091e",
      "#b060db",
      "#3b44b3",
      "#ca4883",
      "#2a59fc",
      "#19e957",
      "#cf1a26",
      "#f5848c",
      "#55a6f7",
      "#29698a",
      "#01a17c",
      "#bb604d",
      "#77c404",
      "#5d0a55",
      "#186626",
      "#65a3c4",
      "#fbfc4a",
      "#e37b43",
      "#1846e3",
      "#f65457",
      "#fe69b5",
      "#6A2135",
      "#E38627",
      "#C13C37",
    ];

    let data = this.props.pieValues.map((saleCard, index) => ({
      title: removeQoutations(saleCard["tenderType"]),
      color: colours[index],
      value: parseInput(saleCard["amt"]),
    }));

    return (
      <>
        <PieChart
          className="box a"
          animate
          animationDuration={500}
          animationEasing="ease-out"
          center={[300, -50]}
          data={data}
          lengthAngle={360}
          //lineWidth={100}
          //paddingAngle={0}
          radius={250}
          //rounded
          startAngle={0}
          viewBoxSize={[800, 10]}
          label={(data) => data.dataEntry.value}
          //labelPosition={65}
          // labelStyle={{
          //   fontSize: "5px",
          //   fontColor: "FFFFFA",
          //   fontWeight: "800",
          // }}
          paddingAngle={0}
          labelStyle={{
            fontSize: "25px",
            fill: "#000",
          }}
          labelPosition={105}
          lineWidth={40}
        />
        <div className="box b">
          {data.map((displayData) => (
            <span
              className="pieStatLegend"
              key={displayData.title}
              style={{ color: displayData.color }}
            >
              &#9632; {displayData.title}
            </span>
          ))}
          {}
        </div>
      </>
    );
  }
}

export default ChartFragment;
