import React from "react";
import Card from "./card";
import "./cardList.css";

//Holds a number of cards in a list, fed from json data.
class CardList extends React.Component {
  render() {
    let array = this.props.cardData.map((card, index) => (
      <Card
        key={index}
        //id={card.Item}
        itemName={card['"Item"']}
        openAmount={card['"OPEN "']}
        del={card['"\'+DEL"']}
        left={card['"\'-LEFT"']}
        used={card['"\'=USED"']}
        posDiff={card['"POS DIFF"']}
        cost={card['"COST"']}
        usedCost={card['"$ =USED"']}
        diffCost={card['"$ DIFF"']}
        percentUsage={card['"ACT %"']}
        percentExpected={card['"EXP %"']}
        percentDiff={card['"% DIFF']}
      ></Card>
    ));
    return <div className="cardList">{array}</div>;
  }
}

export default CardList;
