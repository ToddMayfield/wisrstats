import React from "react";
import SaleCard from "./saleCard";
import "./saleCardList.css";

//Holds a number of cards in a list, fed from json data.
class SaleCardList extends React.Component {
  render() {
    let array = this.props.saleCardData.map((saleCard, index) => (
      <SaleCard
        key={index}
        id={saleCard["tenderType"]}
        itemName={saleCard["tenderType"]}
        saleAmount={saleCard["amt"]}
        totalSales={saleCard["amt2\r"]}
        percentageOfTotal={saleCard["Percentage"]}
      ></SaleCard>
    ));
    return <div className="saleCardList">{array}</div>;
  }
}

export default SaleCardList;
